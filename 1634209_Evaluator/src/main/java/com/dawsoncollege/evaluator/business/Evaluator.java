package com.dawsoncollege.evaluator.business;

import com.dawsoncollege.evaluator.errors.DividedByZeroException;
import com.dawsoncollege.evaluator.errors.InvalidStringArgumentException;
import com.dawsoncollege.evaluator.errors.MissingParenthesesException;
import com.dawsoncollege.evaluator.errors.NonBinaryExpressionException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.Queue;

/**
 * Class for performing the whole process of transforming an operation, in Queue of String, 
 * turn it from infix to postfix and get the result 
 * @author Alexa
 */
public class Evaluator {
    
    private final Deque<String> operatorStack;
    private final Queue<String> operandAndOperatorQueue;
    
    /**
     * Constructor with no param, the operator and queue are instantiated
     */
    public Evaluator(){
        this.operatorStack = new ArrayDeque<String>();
        this.operandAndOperatorQueue = new ArrayDeque<String>();
    }
   
   /**
     * Heart of the Evaluator class. This will receive a queue representing the operation, 
     * will evaluate it to see if it is valid, throwing the appropriate custom exception if it isn't,
     * while transforming it from infix to postfix. Afterwards, it will return the result, a double,
     * when it is all said and done
     * 
     * @param operation
     * @return
     * @throws MissingParenthesesException
     * @throws InvalidStringArgumentException
     * @throws NonBinaryExpressionException
     * @throws DividedByZeroException 
     */
    public double infixToPostfixToResult(Queue<String> operation) throws MissingParenthesesException, InvalidStringArgumentException, NonBinaryExpressionException, DividedByZeroException{
        //We already assume at the beginning that the Deque has been built correctly
        //will evaluate while doing the process of calculating
        String previousChar=""; 
        int count=0; 
        int countNonBinary=0;
        int unclosedParenthesis=0;
        for(String value: operation){
            value = value.replaceAll("\\s+","");
            if(checkIfNull(value) || value.equals(""))
                throw new InvalidStringArgumentException("It seems input is empty");
            if(!onlyNumbersAndOperatorsAllowed(value)) {
            	throw new InvalidStringArgumentException("It seems input contains other than operands and operators");
            }
           
            //go here if the value is an operator
            if(isAnOperator(value)){
                if(isAnOperator(previousChar)){
                    throw new NonBinaryExpressionException("can't have two operators following each other");
                }
                if(count==0 && operation.size()>1){
                    throw new NonBinaryExpressionException("your operation starts with an operator");
                }
                if(previousChar.equals("(")){
                    throw new NonBinaryExpressionException("your operation has an operator preced with a open parenthesis");
                }
                //Now check if the operator has less precedence than the one in 
                //the Stack if it is not empty of course
                
                if(!operatorStack.isEmpty()){
                    // set Iterator as descending using descendingIterator() method for the stack operator
                    Iterator<String> iterator = operatorStack.iterator();
                    changeOperatorStack(value,iterator);
                    
                } else {
                   operatorStack.push(value);
                   countNonBinary++;
                }
            }
            
            //Go here if value is a number
            if(isANumber(value)){
                String[] splitValue = value.split("");
                String newValue=""; 
                int countChar=0;
                for (String splitValueChar : splitValue) {
                    if(splitValue[0].equals("-")){
                        if (isANumber(splitValueChar)){
                            newValue += splitValueChar;
                        } else if (splitValueChar.equals("-") && countChar==0){
                            newValue += splitValueChar;
                        } else if (splitValueChar.equals(".") && countChar!=1 && countChar != splitValue.length-1){
                            newValue += splitValueChar;
                        } else if(countChar == splitValue.length-1) {
                            value = newValue;
                        } else {
                            throw new InvalidStringArgumentException("It seems input contains more than a single character that doens't make it as a number");
                        }
                        countChar++;
                    }else if (isAnOperator(splitValueChar)) {
                        throw new InvalidStringArgumentException("It seems input contains more than a single character that doens't make it as a number");
                    }
                }
            	if(isANumber(previousChar)) {
            	    throw new NonBinaryExpressionException("your operation doens't make sense, a number can't have a number or closing paranthesis beforehand");
                
                }else if (previousChar.equals(")")){
                    pushHiddenMultiplication(value,previousChar);
                }else {
                    operandAndOperatorQueue.offer(value);
                    countNonBinary++;
            	}
            }
            
            //Go here if value is an opening parenthesis "("
            if(value.equals("(")){//add open parenthesis in stack operator, will act as a limit for where the popping can go
                pushHiddenMultiplication(value,previousChar);
                unclosedParenthesis++;
            }
            
            //Go here if value is a closing parenthesis ")"
            if(value.equals(")")){
                if(isAnOperator(previousChar)){
                    throw new NonBinaryExpressionException("in your operation, you have an operator followed by a closing paranthesis");
                }
                unclosedParenthesis--;
                
            }
            previousChar=value;
            count++;
            if(isAnOperator(value) && count==operation.size()) {
            	throw new NonBinaryExpressionException("operation finishes with an operator");
            }
        }//end for loop
        
        
        //Check that the rule of parenthesis is respected and throw an exception if not 
        if(unclosedParenthesis!=0){
            throw new MissingParenthesesException("Seems that you are missing some parantheses");
        }
        
        //extract the values from the operator stack and put it in the queue
        int stackSize = operatorStack.size();
        for(int i=0;i<stackSize;i++){
            String operator = operatorStack.pop();
            if(!operator.equals("(")) {//don't add the ( in the queue
                 operandAndOperatorQueue.offer(operator);
            }
        }
        
        //check that there is not only one number or operator in the queue, throw an exception otherwise
        if(countNonBinary==1){
            throw new NonBinaryExpressionException("Only one number or operator in operation");
        }
        
        //Step2: Now we will calculate the result by using a new stack 
        //and a queue of 3 space. the queue with operators and operands should be full
        String result = findTheResult();
        if(result == null) {
            throw new NonBinaryExpressionException("Stack became empty when popped one value, when it should be two");
        }
        return Double.parseDouble(result);
    }
	
    /**
     * checking if the String passed is null or not
     * @param c
     * @return 
     */
    private boolean checkIfNull(String c){
	return(c == null);
    }
	
 
    /**
     * check if given character is an arithmetic operator
     * @param c
     * @return 
     */
    private boolean isAnOperator(String c){
        return (c.equals("+") || c.equals("-") || c.equals("*") || c.equals("/") || c.equals("^"));
    }    
    
    /**
     * check if the following String is a valid number. Warning, "4+6-7" 
     * for example would be considered a number
     * @param c
     * @return 
     */
    private boolean isANumber(String c){
        return (c.matches("[-+]?\\d*\\.?\\d+"));
    }
    
    /**
     * Returns a boolean depending on if the String is a number, operator, opening or closing parenthesis 
     * @param c
     * @return 
     */
    private boolean onlyNumbersAndOperatorsAllowed(String c){
    	if(isAnOperator(c) || isANumber(c) || c.equals("(") || c.equals(")")) {
    		return true;
    	}
    	return false;
        //return Pattern.matches("[a-zA-Z]+",maybeNumber);
    }
    
    /**
     * If there is a number that is preceded or followed by a ) or ( respectively,
     * push a * in the stack operator
     * @param value
     * @param previousChar 
     */
    private void pushHiddenMultiplication(String value, String previousChar){
        if(isANumber(previousChar) || previousChar.equals(")")) {
            //in the case that somebody writes something like 6(2+3), 
            //need to add a multiplication in the operator stack
            if(!operatorStack.isEmpty()){
                // set Iterator as descending using descendingIterator() method for the stack operator
                Iterator<String> iterator = operatorStack.iterator();
                changeOperatorStack("*",iterator);    
            }else {
                operatorStack.push("*");	
            }
        }
        operatorStack.push(value);
    }
    
    /**
     * It will receive the operator to see if it should be pushed right away into the Stack operator
     * or that it has less precedence than the ones in the stack, therefore the latter will be put in 
     * the queue of operands and operators and will continue the process with the iterator until 
     * conditions are met
     * @param value
     * @param iterator 
     */
    private void changeOperatorStack(String value, Iterator<String> iterator) {
         switch(value) {
            case "^":
        	 //dealing with exponent
        	 operatorStack.push(value);
        	 break;
        	 
            case "/":
        	 //dealing with division
        	 while(iterator.hasNext()){
        		 String newOperator = iterator.next();
                 if(newOperator.equals("^")) {
                   //pop the exponent off stack and add it to the queue
                	 String exponant = operatorStack.pop();
                	 operandAndOperatorQueue.offer(exponant);
                 } else if(newOperator.equals("(") || newOperator.equals("/") || 
                		 newOperator.equals("*") || newOperator.equals("-") || 
                		 newOperator.equals("+")) {
                	 //Division has a higher precedence than the other operators
                	 //and ( acts implicitly as a new stack (a new stop)
                	 operatorStack.push(value);
                	 break;
                 }
             }//end while loop
        	 //in case that stack is empty after popping, push the operator
        	 if(operatorStack.isEmpty())
				operatorStack.push(value);
        	 break;
        	 
            case "*":
        	 //dealing with multiplication
        	 while(iterator.hasNext()){
        		 String newOperator = iterator.next();
                 if(newOperator.equals("^") || newOperator.equals("/")) {
                   //pop the exponent or division off stack and add it to the queue
                	 String operator = operatorStack.pop();
                	 operandAndOperatorQueue.offer(operator);
                 } else if(newOperator.equals("(") || newOperator.equals("*")
                		 || newOperator.equals("-") || newOperator.equals("+")) {
                	//Multiplication has a higher precedence than the other operators
                	//mentioned above and ( acts implicitly as a new stack (a new stop)
                	 operatorStack.push(value);
                	 break;
                 }
        	 }//end while loop
        	//in case that stack is empty after popping, push the operator
        	 if(operatorStack.isEmpty())
				operatorStack.push(value);
        	 break;
        	 
            case "-":
        	 //dealing with subtraction
        	 while(iterator.hasNext()){
        		 String newOperator = iterator.next();
                 if(newOperator.equals("^") || newOperator.equals("/")
                		 || newOperator.equals("*")) {
                   //pop the exponent, division or multiplication off stack and add it to the queue
                	 String operator = operatorStack.pop();
                	 operandAndOperatorQueue.offer(operator);
                 } else if(newOperator.equals("(") || newOperator.equals("-")
                		 || newOperator.equals("+")) {
                	//Subtraction has a higher precedence than the other operators
                	//mentioned above and ( acts implicitly as a new stack (a new stop)
                	 operatorStack.push(value);
                	 break;
                 }
        	 }//end while loop
        	//in case that stack is empty after popping, push the operator
        	 if(operatorStack.isEmpty())
				operatorStack.push(value);
        	 break;
        	 
            default://+
        	 //dealing with addition
        	 while(iterator.hasNext()){
        		 String newOperator = iterator.next();
                 if(newOperator.equals("^") || newOperator.equals("/")
                		 || newOperator.equals("*") ||newOperator.equals("-")) {
                   //pop the exponent, division or multiplication off stack and add it to the queue
                	 String operator = operatorStack.pop();
                	 operandAndOperatorQueue.offer(operator);
                 } else if(newOperator.equals("(") || newOperator.equals("+")) {
                	//Addition has the lowest precedence than the other operators
                	//and ( acts implicitly as a new stack (a new stop)
                	 operatorStack.push(value);
                	 break;
                 }
        	 }//end while loop
        	//in case that stack is empty after popping, push the operator
        	 if(operatorStack.isEmpty())
				operatorStack.push(value);
        	 break;
         }//end switch statement
    }
    
    
    
    /**
     * Extract the values from the operand and operator queue to find the result by
     * putting in a new stack which when an operator has focus, put it in
     * the queue of 3 space and perform the small operation
     * @return
     * @throws DividedByZeroException 
     */
    private String findTheResult() throws DividedByZeroException {
    	double result=0;
    	Queue<String> smallOperation = new ArrayDeque<>(3);
    	Deque<String> boilingDownToResult = new ArrayDeque<>();
    	
    	//poll the values progressively from the operandAndOperatorQueue
    	int sizeQueue = operandAndOperatorQueue.size();
    	for(int i=0;i<sizeQueue;i++) {
    	    String element = operandAndOperatorQueue.poll();
    	    if(isANumber(element)) {
    		//a number, therefore put it in the Stack
    		boilingDownToResult.push(element);
    	    }else if(isAnOperator(element)) {
    		if(boilingDownToResult.isEmpty()) {
    		    return null;
    		}
    		String secondValue = boilingDownToResult.pop();
    		smallOperation.offer(secondValue);
    	        smallOperation.offer(element);
    	        if(boilingDownToResult.isEmpty()) {
    		    return null;
    		}
    	        smallOperation.offer(boilingDownToResult.pop());   
    	        result = performSmallOperation(smallOperation);
    	        if(!operandAndOperatorQueue.isEmpty()) {
    	            boilingDownToResult.push(Double.toString(result));
    	        }else {
    	    	    return Double.toString(result);
    	       }
    	   }
    	}
    	return Double.toString(result);
    }
    
    /**
     * perform the small operation by using a switch statement to find which 
     * operator to use and return the result
     * @param smallOperation
     * @return
     * @throws DividedByZeroException 
     */
    private double performSmallOperation(Queue<String> smallOperation)throws DividedByZeroException{
    	double result=0;
    	double secondValue = Double.parseDouble(smallOperation.poll());
    	String operation = smallOperation.poll();
    	double firstValue = Double.parseDouble(smallOperation.poll());
    	switch(operation) {
    	case "^":
    		result = Math.pow(firstValue,secondValue);
    		break;
    	case "/":
                //if user tries to divide by zero, throw custom exception
                if(secondValue==0){
                    throw new DividedByZeroException("You can't divide a number by zero");
                }
    		result = firstValue/secondValue;
    		break;
                
    	case "*":
    		result = firstValue*secondValue;
    		break;
    	case "-":
    		result = firstValue-secondValue;
    		break;
    	default://+
    		result = firstValue+secondValue;
    		break;
    	}
    	return result;
    } 
}//end ISPEvalatorImpl class
