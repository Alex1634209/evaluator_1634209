package com.dawsoncollege.evaluator.errors;

/**
 * Will throw this exception when somebody tries to divide by zero
 * @author Alexa
 */
public class DividedByZeroException extends Exception{
    /**
     * Constructor of DividedByZeroException which has an error message as param
     * @param errorMessage 
     */
    public DividedByZeroException(String errorMessage){
        super(errorMessage);
    }
}
