package com.dawsoncollege.evaluator.errors;

/**
 * error will be thrown when an invalid String, not intended input for method happens
 * @author Alexa
 */
public class InvalidStringArgumentException extends Exception{
    /**
     * Constructor of InvalidStringArgumentException which has an error message as param
     * @param errorMessage 
     */
    public InvalidStringArgumentException(String errorMessage){
        super(errorMessage);
    }
}
