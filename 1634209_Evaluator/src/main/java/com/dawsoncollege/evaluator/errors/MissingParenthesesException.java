package com.dawsoncollege.evaluator.errors;

/**
 * throw an error when there are missing parentheses in an operation
 * @author Alexa
 */
public class MissingParenthesesException extends Exception{
    /**
     * constructor for MissingParenthesesException which has an error message as param 
     * @param errorMessage 
     */
    public MissingParenthesesException(String errorMessage){
        super(errorMessage);
    }
}
