package com.dawsoncollege.evaluator.errors;

/**
 * throw an error when the operation has only one number or operator in the operation
 * @author Alexa
 */
public class NonBinaryExpressionException extends Exception{
    /**
     * constructor of NonBinaryExpressionException which has an error message as a param
     * @param errorMessage 
     */
    public NonBinaryExpressionException(String errorMessage){
        super(errorMessage);
    }
}
