package com.dawsoncollege.evaluator.unit_test;

import com.dawsoncollege.evaluator.business.Evaluator;
import com.dawsoncollege.evaluator.errors.DividedByZeroException;
import com.dawsoncollege.evaluator.errors.InvalidStringArgumentException;
import com.dawsoncollege.evaluator.errors.MissingParenthesesException;
import com.dawsoncollege.evaluator.errors.NonBinaryExpressionException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Parameterized test class to perform test on the evaluator
 * @author Alexa
 */
@RunWith(Parameterized.class)
public class EvaluatorTest {
    
    /**
     * A static method is required to hold all the data to be tested, the
     * expected result and an error to expect, otherwise null. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11
     * feature
     *
     * @return The list of arrays
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("(","3","/","2",")","(","3","/","6",")")), 0.75,null},
            {new ArrayDeque<>(Arrays.asList("12","-","4","+","7","^","0","*","5","/","1","+","5","-","2","+","1","-","1")),16.0,null},
            {new ArrayDeque<>(Arrays.asList("(","3","/","2",")","+","(","3","/","6",")")),2.0,null},
            {new ArrayDeque<>(Arrays.asList("5","(","3","^","6",")")),3645.0,null},
            {new ArrayDeque<>(Arrays.asList(".45","/","0.35","*","0.15","^",".5")),0.49795500165523937,null},
            {new ArrayDeque<>(Arrays.asList("2","*","9","/","3","+","6","-","2","/","4")),11.5,null},
            {new ArrayDeque<>(Arrays.asList("12","-","4","+","7","^","0","*","5","/","1")),13.0,null},
            {new ArrayDeque<>(Arrays.asList("4","/","2","*","3","-","4","/","2","+","6","/","3")),6.0,null},
            {new ArrayDeque<>(Arrays.asList("6","^","2","+","-1.1")),34.9,null},
            {new ArrayDeque<>(Arrays.asList("(","1","+","4",")","5")),21.0,null},
            {new ArrayDeque<>(Arrays.asList("3","(","1","+","4",")","5")),63.0,null},
            {new ArrayDeque<>(Arrays.asList("3","/","0")),0,DividedByZeroException.class},
            {new ArrayDeque<>(Arrays.asList("(","3","^","3",")","/","0")),0,DividedByZeroException.class},
            {new ArrayDeque<>(Arrays.asList("(","7","^","9","/","0",")")),0,DividedByZeroException.class},
            {new ArrayDeque<>(Arrays.asList("(","3","(","4","/","2",")")),0,MissingParenthesesException.class},
            {new ArrayDeque<>(Arrays.asList("3","(","4","/","2",")",")")),0,MissingParenthesesException.class},
            {new ArrayDeque<>(Arrays.asList("(","(","(","4","/","2",")",")")),0,MissingParenthesesException.class},
            {new ArrayDeque<>(Arrays.asList("3")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("*")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("(","3",")")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("(","*",")")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("4","*","+","6")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("+","5","-","8")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("1","+","4","^")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("9","5","-","8")),0,NonBinaryExpressionException.class},
            {new ArrayDeque<>(Arrays.asList("f","-","3")),0,InvalidStringArgumentException.class},
            {new ArrayDeque<>(Arrays.asList("1","/","@")),0,InvalidStringArgumentException.class},
            {new ArrayDeque<>(Arrays.asList("5+6-4")),0,InvalidStringArgumentException.class},
            {new ArrayDeque<>(Arrays.asList("5.8.9","-","3")),0,InvalidStringArgumentException.class},
            {new ArrayDeque<>(Arrays.asList("9,4","-","3")),0,InvalidStringArgumentException.class}
        });
    }
    
    // A Rule is implemented as a class with methods that are associared
    // with the lifecycle of a unit test. These methods run when required.
    // Avoids the need to cut and paste code into every test method.
    @Rule
    public MethodLogger methodLogger = new MethodLogger();
    
    private final Queue<String> operation;
    private final double expResult;
    private final Class<? extends Exception> exception;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Constructor that receives all the data for each test as defined by a row
     * in the list of parameters
     * @param operation
     * @param expResult
     * @param exception 
     */
    public EvaluatorTest(ArrayDeque<String> operation, double expResult, Class<? extends Exception> exception) {
        this.operation = operation;
        this.expResult = expResult;
        this.exception = exception;
    }
    
    /**
     * Test of infixToPostfix method, of class Evaluator. ExpectedException expects an
     * exception to be thrown at some point if the third argument of array isn't null
     * @throws java.lang.Exception
     */
    @Test
    public void testInfixToPostfixToResult() throws Exception{
        System.out.println("infixToPostfixToResult, expected result: "+expResult);
        if(exception != null)
            thrown.expect(exception);
       
        Evaluator evaluator = new Evaluator();
        assertEquals(expResult,evaluator.infixToPostfixToResult(operation),0.01);
    }
}
